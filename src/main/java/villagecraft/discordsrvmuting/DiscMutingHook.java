package villagecraft.discordsrvmuting;

import github.scarsz.discordsrv.api.ListenerPriority;
import github.scarsz.discordsrv.api.Subscribe;
import github.scarsz.discordsrv.api.events.DiscordGuildMessagePostProcessEvent;
import github.scarsz.discordsrv.dependencies.jda.api.entities.User;
import github.scarsz.discordsrv.dependencies.kyori.adventure.text.Component;
import github.scarsz.discordsrv.dependencies.kyori.adventure.text.ComponentBuilder;
import github.scarsz.discordsrv.dependencies.kyori.adventure.text.event.ClickEvent;
import github.scarsz.discordsrv.dependencies.kyori.adventure.text.event.HoverEvent;
import github.scarsz.discordsrv.dependencies.kyori.adventure.text.format.NamedTextColor;
import github.scarsz.discordsrv.dependencies.kyori.adventure.text.format.TextColor;
import github.scarsz.discordsrv.hooks.chat.ChatHook;
import github.scarsz.discordsrv.util.MessageUtil;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.*;
import java.util.stream.Collectors;

public class DiscMutingHook implements ChatHook{

    private Map<Component, User> messageMap = new HashMap<>();
    private final DiscordSRVMuting discordSRVMuting;
    private static QDCircularQueue<User> last10Users = new QDCircularQueue<>(10);

    public DiscMutingHook(DiscordSRVMuting discordSRVMuting) {
        this.discordSRVMuting = discordSRVMuting;
    }

    @Override
    public Plugin getPlugin() {
        return discordSRVMuting;
    }

    @Override
    public void broadcastMessageToChannel(String channel, String message) {
        broadcastMessageToChannel(channel, MessageUtil.toComponent(message));
    }

    @Override
    public void broadcastMessageToChannel(String channel, Component message) {
        Long senderID = messageMap.remove(message).getIdLong();
        if(senderID == null){
            senderID = 0L;
        }
        Long finalSenderID = senderID;
        Collection<? extends Player> recipients  = Bukkit.getOnlinePlayers().stream().filter((p) -> PlayerData.get(p).testToSendMessage(finalSenderID)).collect(Collectors.toList());

        message = generateIgnoreUserButton(finalSenderID).append(message.color(NamedTextColor.WHITE));

        MessageUtil.sendMessage(recipients, message);
    }

    @Subscribe(priority = ListenerPriority.MONITOR)
    public void onDiscordMsgPPEvent(DiscordGuildMessagePostProcessEvent event){
        User author = event.getAuthor();
        Component message = event.getMinecraftMessage();
        messageMap.put(message, author);

        /* now save user to userQueue */
        if(!last10Users.contains(author)){
            last10Users.add(author);
        }

    }

    public static User[] getLast10Senders(){
        return last10Users.toArray(new User[10]);
    }

    private Component generateIgnoreUserButton(long userID){
        Component out = Component.text("x")
        .clickEvent(ClickEvent.runCommand("/ignoreuser " + Long.toString(userID) + " ignore"))
        .hoverEvent(HoverEvent.showText(Component.text("Ignore this discord User")))
        .color(NamedTextColor.DARK_RED);
        return out;
    }

}
