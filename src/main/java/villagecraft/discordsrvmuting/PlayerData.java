package villagecraft.discordsrvmuting;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class PlayerData {

    private static Map<Player, PlayerData> playerDataMap = new HashMap<>();

    private boolean DiscordIgnored;
    private final List<Long> ignoredPlayerIDs;

    protected PlayerData(@NotNull Player player){

        ignoredPlayerIDs = new ArrayList<>();
        DiscordIgnored = false;

        playerDataMap.put(player, this);
    }

    protected PlayerData(@Nullable ConfigurationSection conf, @NotNull Player player){
        DiscordIgnored = conf.getBoolean("conf.discordIgnored", false);
        ignoredPlayerIDs = new ArrayList<>();
        ignoredPlayerIDs.addAll(conf.getLongList("conf.ignoredIDs"));
        playerDataMap.put(player, this);
    }

    public ConfigurationSection saveToConfigurationSection(@NotNull ConfigurationSection conf){
        conf.set("conf.discordIgnored", DiscordIgnored);
        conf.set("conf.ignoredIDs", ignoredPlayerIDs);
        return conf;
    }

    public boolean checkIgnored(Long senderID){
        return ignoredPlayerIDs.contains(senderID);
    }

    public void ignoreDiscordID(Long discID){
        ignoredPlayerIDs.add(discID);
    }

    public void unIgnoreDiscordID(Long discID){
        ignoredPlayerIDs.remove(discID);
    }

    public boolean isDiscordIgnored() {
        return DiscordIgnored;
    }

    public void setDiscordIgnored(boolean discordIgnored) {
        DiscordIgnored = discordIgnored;
    }

    public boolean testToSendMessage(Long discID){
        return !(checkIgnored(discID) || isDiscordIgnored());
    }

    public List<Long> getIgnoredPlayerIDs(){
        return Collections.unmodifiableList(ignoredPlayerIDs);
    }

    @Nullable
    public static PlayerData get(@NotNull Player player){
        return playerDataMap.get(player);
    }

    @Nullable
    public static PlayerData remove(@NotNull Player player){
        return playerDataMap.remove(player);
    }

}
