package villagecraft.discordsrvmuting;

import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Guild;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Member;
import github.scarsz.discordsrv.dependencies.jda.api.entities.User;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.List;

public class GenericUtils {

    @Nullable
    public static Member getMemberByString(String name){
        User[] last10Senders = DiscMutingHook.getLast10Senders();
        Guild guild = DiscordSRV.getPlugin().getMainGuild();
        for (User u:
                last10Senders) {
            if(u == null){
                continue;
            }
            if(u.getName().equals(name)){
                Member member = guild.getMemberById(u.getIdLong());
                if(member != null){
                    return member;
                }
            }
        }
        List<Member> candidates = Collections.EMPTY_LIST;
        candidates = guild.getMembersByNickname(name, false);
        if(candidates.size() == 0){
            candidates = guild.getMembersByName(name, false);
        }
        return candidates.size() > 0 ? candidates.get(0) : null;
    }

}
