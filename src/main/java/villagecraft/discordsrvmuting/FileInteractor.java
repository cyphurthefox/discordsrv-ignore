package villagecraft.discordsrvmuting;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FileInteractor implements Listener {
    private final ExecutorService es;
    private final Map<Player, Future<?>> removing = new HashMap<>();
    private final Map<Player, Future<?>> loading = new HashMap<>();
    private final ArrayList<Player> Loaded = new ArrayList<>();

    private final File dataFolder;

    public FileInteractor(File plDataFolder) {
        this.dataFolder = new File(plDataFolder, "playerdata");
        es = Executors.newSingleThreadExecutor();
        try {
            if(!dataFolder.exists()){
                dataFolder.mkdirs();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent ev){
        load(ev.getPlayer());
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent ev){
        save(ev.getPlayer());
    }
    
    private void load(Player player){
        if(removing.containsKey(player)){
            Future<?> remove = removing.get(player);
            if(remove.cancel(false)){
                removing.remove(player);
            }
        }
        if(Loaded.contains(player)){
            return; //loading alr done
        }
        if(loading.containsKey(player)){
            return; //alr q'd loading task
        }

        loading.put(player, es.submit(() -> {
            File userFile = new File(dataFolder, player.getUniqueId().toString() + ".yml");
            PlayerData nps;
            if(!userFile.exists()){
                /* doesnt exist, nothing to load, get a fresh one */
                nps =  new PlayerData(player);
                Bukkit.getLogger().info("[CyphurChat] playerData for " + player.getName() + " was not found, so a new file was generated");
            } else {
                nps =  new PlayerData(YamlConfiguration.loadConfiguration(userFile), player);
            }

            Loaded.add(player);
            loading.remove(player); //keepin things clean
            Bukkit.getLogger().info("[CyphurChat] Sucessfully loaded playerData for " + player.getName() + " UUID: " + player.getUniqueId());
            return; //PACK IT UP BOYS
        }));
    }
    
    public void save(Player player){
        if(loading.containsKey(player)){
            Future<?> load = loading.get(player); //see if we're loading this player's data when they leave (unlikely but possible)
            if(load.cancel(false)){ //try to cancel it, but not if its alr running. if its enqueued but not running we'll return true and the load will never happen, saving compute.
                loading.remove(player); //if we stopped while it was queued we can fahgettaboutit
                return; //no need to save anything, if it was going to load, there's nothing to save.
            }
        }
        if(removing.containsKey(player)){
            return; //saving task already queued
        }

        //if it wasnt in the load queue, was executing (failed to cancel) or already loaded and not cleaned up yet, then we might have data to save. schedule a task to do so:
        /* playerdata saving task */
        removing.put(player, es.submit(() -> {

            try {
                File userFile = new File(dataFolder, player.getUniqueId().toString() + ".yml"); //get playerdata file
                if(!userFile.exists()){
                    userFile.createNewFile();
                }
                YamlConfiguration config = YamlConfiguration.loadConfiguration(userFile); //load into YamlConfig cuz I want it to be human-readable
                PlayerData.remove(player).saveToConfigurationSection(config); //remove player from map to speed up future searches (also releases the Player Object held in the PlayerSession so the JVM can GC it (this probs causes weird spigot problems anyway)
                config.save(userFile);
                Bukkit.getLogger().info("[CyphurChat] Sucessfully saved playerData for " + player.getName() + " UUID: " + player.getUniqueId());
                removing.remove(player); //keepin it clean
            } catch (Exception e) {
                e.printStackTrace();
            }
            return; //WE BE DONE HERE
        }));
    }

    protected void cleanup(){
        Bukkit.getOnlinePlayers().forEach((p) -> save(p));
        es.shutdown();
    }

}
