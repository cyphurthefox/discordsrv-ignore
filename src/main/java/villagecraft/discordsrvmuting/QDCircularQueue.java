package villagecraft.discordsrvmuting;

import java.lang.reflect.Array;
import java.util.Arrays;

public class QDCircularQueue<V> {
    private V[] loop;
    private int Index = 0;
    private final int size;

    public QDCircularQueue(int size) {
        loop = (V[]) new Object[size];
        this.size = size;
    }

    public boolean contains(V obj){
        for (V v :
                loop) {
            if (v == null) {
                continue;
            }
            if(v.equals(obj)){
                return true;
            }
        }
        return false;
    }

    public void add(V obj){
        loop[Index] = obj;
        Index++;
        if(Index >= size){
            Index = 0;
        }
    }

    public V[] toArray(V[] srcArray) {
        V[] buf = (V[]) Array.newInstance(srcArray.getClass().getComponentType(), size);
        System.arraycopy(this.loop, 0, buf, 0, size);
        return buf;
    }

    public Object[] toArray(){
        Object[] buf = new Object[size];
        System.arraycopy(this.loop, 0, buf, 0, size);
        return buf;
    }
}
