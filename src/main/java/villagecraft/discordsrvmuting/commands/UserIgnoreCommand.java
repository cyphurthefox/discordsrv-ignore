package villagecraft.discordsrvmuting.commands;

import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Guild;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Member;
import github.scarsz.discordsrv.dependencies.jda.api.entities.User;
import github.scarsz.discordsrv.util.DiscordUtil;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import villagecraft.discordsrvmuting.DiscMutingHook;
import villagecraft.discordsrvmuting.GenericUtils;
import villagecraft.discordsrvmuting.PlayerData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UserIgnoreCommand implements TabExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command command, String label, String[] args) {
        PlayerData player;
        if(!(cs instanceof Player)){
            cs.sendMessage(ChatColor.RED + "You must be a PLAYER to use this command.");
            return true;
        } else {
            player = PlayerData.get((Player) cs);
        }

        if(args.length == 0){
            cs.sendMessage(ChatColor.RED + "Please provide an Argument to the command!");
            cs.sendMessage(ChatColor.RED + "Usage: /" + label + " <userID | user Nickname | list");
            return true;
        }

        if(args[0].equals("list")){
            cs.sendMessage(ChatColor.DARK_AQUA + "========== Currently Ignored Discord Users ==========");
            if (player.getIgnoredPlayerIDs().size() == 0){
                cs.sendMessage(ChatColor.RED + "You don't have any Discord Users Ignored!");
                return true;
            }
            Guild guild = DiscordSRV.getPlugin().getMainGuild();
            for (Long id:
                 player.getIgnoredPlayerIDs()) {
                Member potMember = guild.getMemberById(id);
                String username = potMember == null ? ChatColor.ITALIC + "Unknown" : potMember.getEffectiveName();
                cs.sendMessage(new ComponentBuilder().append(getUnIgnoreButton(id)).append(id.toString() + "(").color(net.md_5.bungee.api.ChatColor.AQUA).append(username).append(")").color(net.md_5.bungee.api.ChatColor.AQUA).create());
            }
            cs.sendMessage(ChatColor.DARK_AQUA + "=====================================================");
            return true;
        } else {
            long potentialID = 0L;
            try{
                potentialID = Long.parseLong(args[0]);
            } catch (NumberFormatException ex){}

            if(potentialID != 0){
                if(args.length > 1 && args[1].equals("allow")){
                    player.unIgnoreDiscordID(potentialID);
                    cs.sendMessage(ChatColor.GREEN + "You are now " + ChatColor.DARK_GREEN +"recieving" + ChatColor.GREEN + " messages from " + ChatColor.LIGHT_PURPLE + prettyID(potentialID));
                } else if(args.length > 1 && args[1].equals("ignore")){
                    player.ignoreDiscordID(potentialID);
                    cs.sendMessage(ChatColor.GREEN + "You are now " + ChatColor.RED +"ignoring" + ChatColor.GREEN + " messages from " + ChatColor.LIGHT_PURPLE + prettyID(potentialID));
                } else if(player.checkIgnored(potentialID)){
                    player.unIgnoreDiscordID(potentialID);
                    cs.sendMessage(ChatColor.GREEN + "You are now " + ChatColor.DARK_GREEN +"recieving" + ChatColor.GREEN + " messages from " + ChatColor.LIGHT_PURPLE + prettyID(potentialID));
                } else {
                    player.ignoreDiscordID(potentialID);
                    cs.sendMessage(ChatColor.GREEN + "You are now " + ChatColor.RED +"ignoring" + ChatColor.GREEN + " messages from " + ChatColor.LIGHT_PURPLE + prettyID(potentialID));
                }
                cs.sendMessage(getShowListButton());
                return true;
            }
            /* checking by ID failed, try by name? */
            Member member = GenericUtils.getMemberByString(args[0]);
            if (member == null) {
                cs.sendMessage(ChatColor.RED + "Could not find a Discord User by Identifier \"" + args[0] + "\"");
            } else {
                if(player.checkIgnored(member.getIdLong())){
                    player.unIgnoreDiscordID(member.getIdLong());
                    cs.sendMessage(ChatColor.GREEN + "You are now " + ChatColor.DARK_GREEN +"recieving" + ChatColor.GREEN + " messages from " + ChatColor.LIGHT_PURPLE + member.getEffectiveName());
                } else {
                    player.ignoreDiscordID(member.getIdLong());
                    cs.sendMessage(ChatColor.GREEN + "You are now " + ChatColor.RED +"ignoring" + ChatColor.GREEN + " messages from " + ChatColor.LIGHT_PURPLE + member.getEffectiveName());
                }
                cs.sendMessage(getShowListButton());
            }

            return true;
        }
    }
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        ArrayList<String> opts = new ArrayList<>();

        if(args.length > 1){
            return Collections.emptyList();
        }
        for (User u : DiscMutingHook.getLast10Senders()){
            if(u != null){
                opts.add(u.getName());
            }
        }
        opts.add("list");
        try{
            long num = Long.parseLong(args[0]);
            opts.add(Long.toString(num));
        } catch (NumberFormatException ex){}
        return opts;
    }

    private TextComponent getUnIgnoreButton(Long id){
        TextComponent tc = new TextComponent("[X]");
        tc.setColor(net.md_5.bungee.api.ChatColor.RED);
        tc.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/ignoreUser " + id.toString() + " allow"));
        tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("Unignore This User")));
        return tc;
    }

    private String prettyID(long id){
        Member member = DiscordUtil.getMemberById(Long.toString(id));
        if(member != null){
            return member.getEffectiveName();
        }
        return Long.toString(id);
    }

    private TextComponent getShowListButton(){
        TextComponent tc = new TextComponent("[See all ignored users]");
        tc.setColor(net.md_5.bungee.api.ChatColor.AQUA);
        tc.setBold(true);
        tc.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/ignoreUser list"));
        tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("/ignoreUser list")));
        return tc;
    }
}
