package villagecraft.discordsrvmuting.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import villagecraft.discordsrvmuting.PlayerData;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DiscordIgnoreCommand  implements TabExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command command, String label, String[] args) {
        PlayerData player;
        if(!(cs instanceof Player)){
            cs.sendMessage(ChatColor.RED + "You must be a PLAYER to use this command.");
            return true;
        } else {
            player = PlayerData.get((Player) cs);
        }

        if(args.length == 0){
            player.setDiscordIgnored(!player.isDiscordIgnored());
            if(player.isDiscordIgnored()){
                cs.sendMessage(ChatColor.GREEN + "You are now " + ChatColor.RED +"ignoring" + ChatColor.GREEN + " messages from Discord");
            } else {
                cs.sendMessage(ChatColor.GREEN + "You are now " + ChatColor.DARK_GREEN +"recieving" + ChatColor.GREEN + " messages from Discord");
            }
            return true;
        }

        switch (args[0]){
            case "check":
                if(player.isDiscordIgnored()){
                    cs.sendMessage(ChatColor.GREEN + "You are currently " + ChatColor.RED +"ignoring" + ChatColor.GREEN + " messages from Discord");
                } else {
                    cs.sendMessage(ChatColor.GREEN + "You are currently " + ChatColor.DARK_GREEN +"recieving" + ChatColor.GREEN + " messages from Discord");
                }
                return true;

            case "ignore":
                player.setDiscordIgnored(true);
                break;
            case "allow" :
                player.setDiscordIgnored(false);
                break;
            default:
                cs.sendMessage(ChatColor.RED + "Argument \"" + args[0] + "\" not Recognized, Are you sure you typed it correctly?");
                return false;
        }
        if(player.isDiscordIgnored()){
            cs.sendMessage(ChatColor.GREEN + "You are now " + ChatColor.RED +"ignoring" + ChatColor.GREEN + " messages from Discord");
        } else {
            cs.sendMessage(ChatColor.GREEN + "You are now " + ChatColor.DARK_GREEN +"recieving" + ChatColor.GREEN + " messages from Discord");
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if(args.length == 1) {
            return Arrays.asList(new String[]{"check", "ignore", "allow"});
        } else {
            return Collections.emptyList();
        }
    }
}
