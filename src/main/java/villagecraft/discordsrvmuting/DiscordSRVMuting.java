package villagecraft.discordsrvmuting;

import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.hooks.PluginHook;
import github.scarsz.discordsrv.listeners.PlayerChatListener;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import villagecraft.discordsrvmuting.commands.DiscordIgnoreCommand;
import villagecraft.discordsrvmuting.commands.UserIgnoreCommand;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

public final class DiscordSRVMuting extends JavaPlugin {

    private DiscMutingHook dmh = new DiscMutingHook(this);
    private FileInteractor fileInteractor;

    @Override
    public void onEnable() {

        saveDefaultConfig();

        if(RegisterMyHook()){
            DiscordSRV.api.subscribe(dmh);

            DiscordIgnoreCommand discordIgnoreCommand = new DiscordIgnoreCommand();
            PluginCommand dIgnoreCmd = getCommand("ignorediscord");
            dIgnoreCmd.setExecutor(discordIgnoreCommand);
            dIgnoreCmd.setTabCompleter(discordIgnoreCommand);

            UserIgnoreCommand userIgnoreCommand = new UserIgnoreCommand();
            PluginCommand uIgnoreCmd = getCommand("ignoreUser");
            uIgnoreCmd.setExecutor(userIgnoreCommand);
            uIgnoreCmd.setTabCompleter(userIgnoreCommand);

            fileInteractor = new FileInteractor(this.getDataFolder());
            getServer().getPluginManager().registerEvents(fileInteractor, this);

            if(getConfig().getBoolean("EnableDiscordChatRemovalFix", false)) {
                getServer().getPluginManager().registerEvents(new PlayerChatListener(), this);
            }

        } else {
            getLogger().severe("Failed to successfully infiltrate the PluginHooks in DiscordSRV. Muting not Enabled.");
            getServer().getPluginManager().disablePlugin(this);
        }

    }

    @Override
    public void onDisable() {
        DiscordSRV.api.unsubscribe(dmh);
        fileInteractor.cleanup();
    }

    public boolean RegisterMyHook(){
        try {
            DiscordSRV discordSRV = DiscordSRV.getPlugin();
            Field discPluginHooks = discordSRV.getClass().getDeclaredField("pluginHooks"); //cut the red wire....
            discPluginHooks.setAccessible(true); //im in

            Object possiblyHashSet = discPluginHooks.get(discordSRV);
                if(possiblyHashSet instanceof HashSet){

                    Set<PluginHook> pluginHookSet = (HashSet<PluginHook>) possiblyHashSet;

                    pluginHookSet.add(dmh);

                    getLogger().info("Successfully registered DiscordSRV Hook");
                    return true;
                } else {
                    getLogger().severe("DiscordSRVMuting failed to properly register chathook <Unknown>");
                    return false;
                }

        } catch (Exception e) {
            getLogger().severe("DiscordSRVMuting failed to properly register chathook <Exception>");
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        sender.sendMessage(ChatColor.RED + "DiscordSRVMuting Failed to successfully start. Please contact your local Administrator about this issue");
        return true;
    }
}
