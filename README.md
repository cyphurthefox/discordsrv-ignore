# DiscordSRV Ignore

DiscordSRV Ignore is a plugin that interfaces with the popular plugin DiscordSRV to allow players to turn off recieving discord messages, or allow players to ignore specific discord Users. This feature hopes to help curb people who use discord chat in an effort to bypass the typical /ignore command in game.

# Usage
### Basic discordignore
    /ignorediscord [check | allow | ignore]
  the `/ignorediscord` command is the basic command for muting discord. Using the command on its own will toggle the "ignored" state of DiscordSRV. The `check` argument allows you to find out what you have your ignored state set to. `allow` or `deny` will enable you to explicitly set your ignored state

### Ignoring a user

    /ignoreuser <userID | Nickname | List>
the `/ignoreuser` command allows a player to specifically choose to not recieve messages in game from specific discord senders. Using the `list` argument allows a user to see all players they are currently ignoring, enabling them to unignore users if necessary. To ignore a user, perform the command and either provide a userID (64 bit number typically represented as an integer) or their nickname in the server to ignore them. 

# Persistency
Playerdata is saved on a player-per-file basis that is saved by a secondary thread when a user logs out, and loaded by the same when the user logs in. This helps insure that playerdata footprint is kept low in memory as the program does not need to load all userdata, and makes manual playerdata editing easy as playerdata files are saved by UUID in the .yml format for human-redability, should that be required.
Saved playerdata consists of whether or not the player is currently ignoring discord, and the list of discord userIDs the player is currently ignoring. 
